var map;                                                            // data returned from GoogleMap's API
var image;                                                          // map marker image icon
var stationName;                                                    // bike station name

function pullGoogleMap(){
/*
    Function called when landing page loads. Function calls
    GoogleMaps API, requests new Google map centred on
    hard-coded latitude and longitute for city of Dublin,
    Ireland. GoogleMaps API data written-out to placeholder
    div.

*/
    var Latlng = new google.maps.LatLng(53.346057,-6.268001);       // map centering coords
    map = new google.maps.Map(document.getElementById('googleMap'),{// writes map to HTML
        center: Latlng,                                             // centres map on specifed coords
        zoom: 12,                                                   // sets map zoom value
        mapTypeId: google.maps.MapTypeId.ROADMAP                    // sets map type
    });
    buildJCDqueryURL();                                             // calls function to query JCD API
}

function buildJCDqueryURL(){
/*
    Function builds and returns URL to be sent to JCDecaux API.

*/
    var cityName='Dublin';                                          // JCD API required city name value
    var jcdURL='https://api.jcdecaux.com/vls/v1/stations';          // JCD API portal URL
    var apiID = '';                                                 // JCD API key id
    var uri = jcdURL + '?contract=' + cityName + '&apiKey=' + apiID;
    
    queryAPI(uri);                                                  // sends URL to GET request function
}

function queryAPI(url){
/*
    Function takes in URI and, on server OK response, sends
    URI via GET request.  Server response (JSON format) is
    parsed and stored in an array variable.

*/
    var xmlhttp = new XMLHttpRequest();                             // creates new GET request
    
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){      // waits for server OK response
            var jsonArray = JSON.parse(xmlhttp.responseText);       // stores parsed JSON response
            pullKeyValuePairs(jsonArray);                           // calls pullKeyValuePairs function
        }
    };
    
    xmlhttp.open('GET', url, true);
    xmlhttp.send();                                                 // sends GET request
    
    return;
}

function pullKeyValuePairs(array){
/*
    Function takes in array comprising parsed JSON data,
    writes HTML table headers to div place-holder, pulls
    out useful key-value pairs and passes them to another
    function.

*/
    var counter = array.length;
    var i;
    
    for(i = 0; i < counter; i++){
        var stationName = array[i].address;                         // station name
        var stationNumber = array[i].number;                        // station ID number
        var stationStatus = array[i].status;                        // station open/ closed status
        var stationCapacity = array[i].bike_stands;                 // station stand/ bike capactiy
        var availableBikes = array[i].available_bikes;              // number of available bikes
        var freeStands = array[i].available_bike_stands;            // number of free bike stands
        var paymentOption = array[i].banking;                       // credit/ debit card facilities
        var stationLatitude = array[i].position.lat;                // stations latitute coordinates
        var stationLongitude = array[i].position.lng;               // stations longitude coordinates
        
        paymentOption = paymentOption.toString();                   // converts bool to string
        paymentOption = stringToTitleCase(paymentOption);           // calls function to convert strings to title case
        stationStatus = stringToTitleCase(stationStatus);
        
        if (paymentOption == 'True'){
            paymentOption = 'Yes';                                  // make bool 'human' readable
        } else {
            paymentOption = 'No';
        }
        
        createMarkers(stationName, stationCapacity,
            availableBikes, freeStands, stationLatitude,
            stationLongitude);                                      // calls function to plot map markers
    }
}

function stringToTitleCase(str){
/*
    Function takes in string
    Returns inputted string converted to title case.

*/
    return str.replace(/\w+/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function createMarkers(stationName, stationCapacity, availableBikes,
    freeStands, stationLatitude, stationLongitude){
/*
    Function takes in vars containing values indicating a
    station's position, bike capacity, number of available
    bikes and some info. text. Ratio of station capacity to
    number of available bikes is calculated and attributed to
    one of three categories.
    
    Function creates var containing URL pointing to icon file.
    Icon file colour cooresponds category of station occupancy
    level. Coloured icons are used as map markers.
    
    Function also uses two inputted vars, containing floating
    point numbers, representing latitute and longitude
    coordinates, used to position markers, indicating bike
    staiton locations.
    
    In addition to adding the markers, function also reads
    inputed string.  String contents are displayed above map
    markers when user selects map marker.

*/
    var oneThirdCapacity = parseInt((stationCapacity / 3 ));                        // parses list obj. as int, calculates 1/3 station capacity
    var twoThirdsThirdCapacity = parseInt(((stationCapacity / 3 ) * 2));            // calculates 2/3 station capacicity
    var markerCoords = new google.maps.LatLng(stationLatitude,stationLongitude);    // consolodates map marker coordinates
    var markerInfoText = '</ul>' +
        '<li> Bike Station Address: ' + stationName + '</li>' +
        '<li> Station capacity: ' + stationCapacity + '</li>' +
        '<li> No. of Bikes Available: ' + availableBikes + '</li>' +
        '<li> No. of Free Bike-Stands: ' + freeStands  + '</li></ul>';              // map marker info. box text as HTML list
    var markerInfoWindow = new google.maps.InfoWindow({
            content: markerInfoText
        });                                                                         // call GoogleMaps method to add info. boxes to markers
    var image;                                                                      // holds map marker icon    
    
    availableBikes = parseInt(availableBikes);
    
    if (availableBikes >= twoThirdsThirdCapacity){
        image = {
             url: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png',
        };                                                                          // green marker if > 2/3 bikes available
    } else if (availableBikes <= oneThirdCapacity){
        image = {
            url: 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
        };                                                                          // red marker if < 1/3 bikes available
    } else {
        image = {
            url: 'https://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
        };                                                                          // yellow marker if between 1/3 & 2/3 bikes available
    }
    
    var marker = new google.maps.Marker({                           // GoogleMaps API method setups map markers
        position: markerCoords,                                     // positions markers using inputted coordinates
        title: stationName,                                         // labels markers using inputted strings
        icon: image                                                 // sets marker icon according to station occupancy rates
    });
    
    marker.setMap(map)                                              // GoogleMaps API method places markers on map
    
    marker.addListener('click', function(){                         // listen-event displays info. boxes on click
       markerInfoWindow.open(map, marker);
    });
}