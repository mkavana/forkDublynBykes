#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import sqlite3
import click

click.disable_unicode_literals_warning = True

sql1 = '''CREATE TABLE IF NOT EXISTS stations(
    created_at DATETIME,
    station_number INT(4),
    address VARCHAR(100),
    bikes INT(3),
    stands INT(3),
    PRIMARY KEY(created_at, address)
    )
'''

sql2 = '''CREATE TABLE IF NOT EXISTS coordinates(
    created_at DATETIME,
    station_number INT(4),
    lat FLOAT(10),
    lng FLOAT(10),
    PRIMARY KEY(station_number)
    )
'''

@click.group()
def cli():
    pass

@cli.command(short_help='initialise database and create tables')
def init_db():
    conn = sqlite3.connect('dublynbykes.db')
    cur = conn.cursor()
    cur.execute(sql1)
    cur.execute(sql2)
    conn.commit()
    conn.close()

if __name__ == '__main__':
    cli()
