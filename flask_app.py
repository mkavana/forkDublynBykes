#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import sqlite3, os.path, requests, datetime, os, sys, json, time
from flask import Flask, render_template, g, current_app, redirect, session, flash, jsonify, request
from flask_paginate import Pagination, get_page_args
import click

click.disable_unicode_literals_warning = True

app = Flask(__name__)
app.config.from_pyfile('app.cfg')


'''
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    |
    |                   C O N F I G.  O P T I O N S
    |
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
'''
DATABASE_FILENAME = app.config['DATABASE_FILENAME']
SECRET_KEY = app.config['SECRET_KEY']
DEBUG_SWITCH = app.config['DEBUG_SWITCH']
JCD_API_ENDPOINT = app.config['JCD_API_ENDPOINT']
JCD_API_CITY_NAME = app.config['JCD_API_CITY_NAME']
JCD_API_KEY = app.config['JCD_API_KEY']
GOOGLEMAPS_KEY = app.config['GOOGLEMAPS_KEY']

@app.before_request
def before_request():
    g.conn = sqlite3.connect('dublynbykes.db')
    g.conn.row_factory = sqlite3.Row
    g.cur = g.conn.cursor()

@app.teardown_request
def teardown(error):
    if hasattr(g, 'conn'):
        g.conn.close()

def get_db():
    """Open DB"""
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE_FILENAME)
        db.row_factory = sqlite3.Row

    return db

@app.teardown_appcontext
def close_connection(exception):
    """Close DB"""
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

    return

def query_db(query, args=(), one=False):
    """Query DB"""
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()

    return (rv[0] if rv else None) if one else rv

'''
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    |
    |                           R O U T E S
    |
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
'''
@app.route('/')
def index():
    # return redirect('/about')
    # return render_template('about.html')
    return redirect('/stations')

@app.route('/about')
def about():
    #return render_template('about.html')
    return redirect('/stations')

@app.route('/stations' , methods = ['POST'])
def stations():
    g.cur.execute('''SELECT COUNT(*) FROM (
        SELECT MAX(created_at)
        FROM stations
        GROUP BY address)
        AS counted''')

    total = g.cur.fetchone()[0]
    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page'
                                          )
    sql = '''SELECT MAX(created_at), * FROM stations GROUP BY address LIMIT {}, {}'''\
        .format(offset, per_page)

    g.cur.execute(sql)
    stations = g.cur.fetchall()
    pagination = get_pagination(page=page,
                                per_page=per_page,
                                total=total,
                                record_name='stations',
                                format_total=True,
                                format_number=True,
                                )

    return render_template('stations.html', stations=stations,
                           page=page,
                           per_page=per_page,
                           pagination=pagination,
                           )

# @app.route('/stations', defaults={'page': 1})
@app.route('/stations/', defaults={'page': 1})
# @app.route('/stations/page-<int:page>/')
@app.route('/stations/page-<int:page>')
def stations_pagination(page):
    g.cur.execute('''SELECT COUNT(*) FROM (
        SELECT MAX(created_at)
        FROM stations
        GROUP BY address)
        AS counted''')
    total = g.cur.fetchone()[0]
    page, per_page, offset = get_page_args()
    sql = '''SELECT MAX(created_at), * FROM stations GROUP BY address LIMIT {}, {}'''\
        .format(offset, per_page)
    g.cur.execute(sql)
    stations = g.cur.fetchall()
    pagination = get_pagination(page=page,
                                per_page=per_page,
                                total=total,
                                record_name='stations',
                                format_total=True,
                                format_number=True,
                                )
    return render_template('stations.html', stations=stations,
                           page=page,
                           per_page=per_page,
                           pagination=pagination,
                           active_url='stations-page-url',
                           )

@app.route('/stations/station/<int:station_number>')
def station_info(station_number):
    return render_template('station_info.html',
                          station_number=station_number
                          )
@app.route('/operations/pull', methods = ['POST'])
def pull():
    """Get data from API"""
    created_at = (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    response = requests.get(JCD_API_ENDPOINT, params={'contract': JCD_API_CITY_NAME,
     'apiKey': JCD_API_KEY})
    #print response
    response_json = response.json()
    write_occupancy_stats_to_database(response_json, created_at)

    flash('Information updated successfully!', 'success')
    return redirect('/stations')

'''
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    |
    |                       F U N C T I O N S
    |
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
'''
def get_css_framework():
    # return current_app.config.get('CSS_FRAMEWORK', 'bootstrap3')
    return app.config['CSS_FRAMEWORK']                              # I changed this

def get_link_size():
    # return current_app.config.get('LINK_SIZE', 'md')
    return app.config['LINK_SIZE']                                  # I changed this

def show_single_page_or_not():
    # return current_app.config.get('SHOW_SINGLE_PAGE', False)
    return app.config['SHOW_SINGLE_PAGE']                           # I changed this

def get_pagination(**kwargs):
    kwargs.setdefault('record_name', 'records')
    return Pagination(css_framework=get_css_framework(),
                      link_size=get_link_size(),
                      show_single_page=show_single_page_or_not(),
                      **kwargs
                      )

def write_occupancy_stats_to_database(json_data, created_at):
    """Writes API response SQLite db"""
    db =  get_db()

    for line in json_data:
        #print line
        station_number = line["number"]
        address = line["address"]
        bikes = line["available_bikes"]
        stands = line["available_bike_stands"]

        db.cursor().execute('''INSERT INTO stations(created_at,
                            station_number, address, bikes, stands)
                            VALUES(?,?,?,?,?)''',
                            (created_at, station_number,
                                address, bikes, stands))
    db.commit()

def write_coordinates_to_database(json_data, created_at):
    """Writes API response SQLite db"""
    db =  get_db()

    for line in json_data:
        json_station_number = line["number"]
        json_lat = line["position"]["lat"]
        json_lng = line["position"]["lng"]
        # db.cursor().execute('''UPDATE coordinates SET station_number = (json_station_number) WHERE station_number = 1''')
    db.commit()

    for line in json_data:
        address = line["address"]
        bikes = line["available_bikes"]
        stands = line["available_bike_stands"]

        db.cursor().execute('''
            INSERT INTO stations(created_at, station_number, address,
             bikes, stands)
            VALUES(?,?,?,?,?)''',
            (created_at, json_station_number, address, bikes, stands))

    db.commit()

'''
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    |
    |               R U N N I N G  T H E  A P P
    |
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
'''
@click.command()
@click.option('--port', '-p', default=5000, help='listening port')
def run(port):
    app.run(debug='DEBUG_SWITCH', port=port)                         # debug true only for dev.

if __name__ == '__main__':
    app.secret_key='SECRET_KEY'                                      # required when sessions are used
    run()