# Dublyn Bykes app

Welcome to the Dublyn Bykes web application (app).

## What is the Dublyn Bykes app?

The Dublyn Bykes app is a Python/ Flask-based RestfulAPI data scraper.

## What does the Dublyn Bykes app do?

The Dublyn Bykes app provides the following information about the [Dublin Bikes service](http://www.dublinbikes.ie) in Dublin, Ireland:

- bike station location details
- bike station occupancy status
- bike availability statistics

## How does the Dublyn Bykes app work?

The Dublyn Bykes app implements simple web-data scraping from the [JCDecaux Dublin Bikes developer RestfulAPI portal](https://developer.jcdecaux.com) in Python and JavaScript.
JSON formatted data are retrieved from the JCDecaux API, stored in an SQLite database, and then rendered on the Dublyn Bykes website.

The Dublyn Bykes app also uses the GoogleMaps and GoogleCharts APIs to visualize bike-station locations and bike availability statistics.

### Credits

The Dublyn Bykes app's webserver is based on [Ryan Jarvinen's Flask micro-framework template for OpenShift](https://github.com/openshift-quickstart/flask-base).

## Requirements

Python [version 2.7](https://www.python.org/download/releases/2.7/) is needed to run the Python script files.

### Packages

The following Python packages are required:

- [sqlite3](https://docs.python.org/3/library/sqlite3.html)
- [flask](https://flask.palletsprojects.com/en/2.0.x/)
- [flask_paginate](https://flask-paginate.readthedocs.io/)

> **Note**:
> Install the required packages using a Python package manager like [pip](https://pypi.org/project/pip/).
>

### API keys

You need keys to access data from the following APIs:

- **JCDecaux API**. Refer to JCDecaux developer's guide [Getting started](https://developer.jcdecaux.com/#/opendata/vls?page=getstarted).
- **Google Maps API**. Refer to the [Google Maps JavaScript API documentation](https://developers.google.com/maps/documentation/javascript/get-api-key).

## Installation

Complete the following steps to install and run the Dublyn Bykes app locally.

1. In a Terminal, clone the Dublyn Bykes' repo into `some-local-directory` by using the following command:

   ```bash
   git clone https://gitlab.com/mkavana/forkDublynBykes.git some-local-directory
   ```

1. Change into `some-local-directory` by using the following command:

   ```bash
    cd some-local-directory/
   ```

1. Create a new local database by using the following command:

   ```bash
   python ./sql.py --init-db
   ```

1. Start the web app by using the following command:

   ```bash
   python ./app.py
   ```

1. In a web browser, navigate to [http://127.0.0.1:5000](http://127.0.0.1:5000).

## Configuration

Complete the following to configure the Dublyn Bykes app to use your APIs keys.

1. In a text editor, open the file `app.cfg`.

1. Locate the line that contains the declaration `JCD_API_KEY = ""`, and enter your JCDecaux API key between the double quotation marks (" ").

1. Locate the line that contains the declaration `GOOGLEMAPS_KEY = ""`, and enter your Google Maps API key between the double quotation marks (" ").

1. Save the file.

1. In a Terminal, restart the Dublyn Bykes app the following command:

   ```bash
   python ./app.py
   ```

## Demo

A live demo of the Dublyn Bykes app is available on [Python Anywhere](https://dublynbykes.pythonanywhere.com/).

## License

This code is public domain under a [CC0 1.0 licence](http://creativecommons.org/publicdomain/zero/1.0/).
